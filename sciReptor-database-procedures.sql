-- -----------------------------------------------------
-- Procedure `truncate_all`
-- -----------------------------------------------------

DROP PROCEDURE IF EXISTS truncate_all;

DELIMITER $$

CREATE PROCEDURE truncate_all()
BEGIN
-- No FKs
  TRUNCATE TABLE `log_table`;
-- FK reads
  TRUNCATE TABLE `reads_VDJ_segments`; 
  TRUNCATE TABLE `reads_constant_segments`; 
  TRUNCATE TABLE `reads_tags`;
-- FK consensus_stats
  ALTER TABLE `reads_VDJ_segments` DROP CONSTRAINT `fk_reads_VDJ_segments_reads_1`;
  ALTER TABLE `reads_constant_segments` DROP CONSTRAINT `fk_reads_constant_segments_reads_1`;
  ALTER TABLE `reads_tags` DROP CONSTRAINT `fk_reads_tags_reads_1`;
  TRUNCATE TABLE `reads`;
  ALTER TABLE `reads_VDJ_segments` ADD CONSTRAINT `fk_reads_VDJ_segments_reads_1` FOREIGN KEY (seq_id) REFERENCES `reads` (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE `reads_constant_segments` ADD CONSTRAINT `fk_reads_constant_segments_reads_1` FOREIGN KEY (seq_id) REFERENCES `reads` (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE `reads_tags` ADD CONSTRAINT `fk_reads_tags_reads_1` FOREIGN KEY (seq_id) REFERENCES `reads` (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE;
-- FK sequences
  ALTER TABLE `reads` DROP CONSTRAINT `fk_reads_consensus_stats_1`;
  TRUNCATE TABLE consensus_stats;
  ALTER TABLE `reads` ADD CONSTRAINT `fk_reads_consensus_stats_1` FOREIGN KEY (consensus_id) REFERENCES consensus_stats (consensus_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  TRUNCATE TABLE igblast_alignment;
  TRUNCATE TABLE mutations;
  TRUNCATE TABLE warnings;
  TRUNCATE TABLE CDR_FWR;
  TRUNCATE TABLE VDJ_segments;
  TRUNCATE TABLE constant_segments;
-- FK events
  ALTER TABLE `consensus_stats` DROP CONSTRAINT `fk_consensus_stats_sequences_1`;
  ALTER TABLE `igblast_alignment` DROP CONSTRAINT `fk_igblast_alignment_sequences_1`;
  ALTER TABLE `mutations` DROP CONSTRAINT `fk_mutations_sequences_1`;
  ALTER TABLE `warnings` DROP CONSTRAINT `fk_warnings_sequences_1`;
  ALTER TABLE `CDR_FWR` DROP CONSTRAINT `fk_CDR_FWR_sequences_1`;
  ALTER TABLE `VDJ_segments` DROP CONSTRAINT `fk_VDJ_segments_sequences_1`;
  ALTER TABLE `constant_segments` DROP CONSTRAINT `fk_constant_segments_sequences_1`;
  TRUNCATE TABLE sequences;
  ALTER TABLE `consensus_stats` ADD CONSTRAINT `fk_consensus_stats_sequences_1` FOREIGN KEY (sequences_seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE `igblast_alignment` ADD CONSTRAINT `fk_igblast_alignment_sequences_1` FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE `mutations` ADD CONSTRAINT `fk_mutations_sequences_1` FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE `warnings` ADD CONSTRAINT `fk_warnings_sequences_1` FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE `CDR_FWR` ADD CONSTRAINT `fk_CDR_FWR_sequences_1` FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE `VDJ_segments` ADD CONSTRAINT `fk_VDJ_segments_sequences_1` FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE `constant_segments` ADD CONSTRAINT `fk_constant_segments_sequences_1` FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  TRUNCATE TABLE flow;  -- add FK flow_meta
-- FK sort
  ALTER TABLE `sequences` DROP CONSTRAINT `fk_sequence_event_1`;
  ALTER TABLE `flow` DROP CONSTRAINT `fk_flow_flow_meta_1`;
  ALTER TABLE `flow` DROP CONSTRAINT `fk_flow_event_1`;
  TRUNCATE TABLE flow_meta;
  TRUNCATE TABLE event;
  ALTER TABLE `sequences` ADD CONSTRAINT `fk_sequence_event_1` FOREIGN KEY (event_id) REFERENCES event (event_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE `flow` ADD CONSTRAINT `fk_flow_flow_meta_1` FOREIGN KEY (channel_id) REFERENCES flow_meta (channel_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE `flow` ADD CONSTRAINT `fk_flow_event_1` FOREIGN KEY (event_id) REFERENCES event (event_id) ON DELETE RESTRICT ON UPDATE CASCADE;
-- FK sample
  ALTER TABLE `flow_meta` DROP CONSTRAINT `fk_flow_meta_sort_1`;
  ALTER TABLE `event` DROP CONSTRAINT `fk_event_sort_1`;
  TRUNCATE TABLE sort;
  ALTER TABLE `flow_meta` ADD CONSTRAINT `fk_flow_meta_sort_1` FOREIGN KEY (sort_id) REFERENCES sort (sort_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE `event` ADD CONSTRAINT `fk_event_sort_1` FOREIGN KEY (sort_id) REFERENCES sort (sort_id) ON DELETE RESTRICT ON UPDATE CASCADE;
-- FK donor
  ALTER TABLE `sort` DROP CONSTRAINT `fk_sort_sample_1`; 
  TRUNCATE TABLE sample;
  ALTER TABLE `sort` ADD CONSTRAINT `fk_sort_sample_1` FOREIGN KEY (sample_id) REFERENCES sample (sample_id) ON DELETE RESTRICT ON UPDATE CASCADE;
-- Only incomings FKs
  ALTER TABLE `sample` DROP CONSTRAINT `fk_sample_donor_1`;
  TRUNCATE TABLE donor;
  ALTER TABLE `sample` ADD CONSTRAINT `fk_sample_donor_1` FOREIGN KEY (donor_id) REFERENCES donor (donor_id) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE `reads` DROP CONSTRAINT `fk_reads_sequencing_run_1`;
  TRUNCATE TABLE sequencing_run;
  ALTER TABLE `reads` ADD CONSTRAINT `fk_reads_sequencing_run_1` FOREIGN KEY (sequencing_run_id) REFERENCES sequencing_run (sequencing_run_id) ON DELETE RESTRICT ON UPDATE CASCADE;
END
$$

DELIMITER ;


-- -----------------------------------------------------
-- Procedure `drop_all`
-- -----------------------------------------------------

DROP PROCEDURE IF EXISTS drop_all;

DELIMITER $$

CREATE PROCEDURE drop_all()
BEGIN
-- No FKs
  DROP TABLE `log_table`;
-- FK reads
  DROP TABLE `reads_VDJ_segments`; 
  DROP TABLE `reads_constant_segments`; 
  DROP TABLE `reads_tags`;
-- FK consensus_stats
  DROP TABLE `reads`;
-- FK sequences
  DROP TABLE `consensus_stats`;
  DROP TABLE `igblast_alignment`;
  DROP TABLE `mutations`;
  DROP TABLE `warnings`;
  DROP TABLE `CDR_FWR`;
  DROP TABLE `VDJ_segments`;
  DROP TABLE `constant_segments`;
-- FK events
  DROP TABLE `sequences`;
  DROP TABLE `flow`;  -- add FK flow_meta
-- FK sort
  DROP TABLE `flow_meta`;
  DROP TABLE `event`;
-- FK sample
  DROP TABLE `sort`;
-- FK donor
  DROP TABLE `sample`;
-- Only incomings FKs
  DROP TABLE `donor`;
  DROP TABLE `sequencing_run`;
END
$$

DELIMITER ;
