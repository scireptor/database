-- -----------------------------------------------------
-- Table `donor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `donor` (
  `donor_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `donor_identifier` VARCHAR(45) NOT NULL,
  `background_treatment` JSON NULL DEFAULT NULL,
  `project` VARCHAR(45) NULL,
  `strain` VARCHAR(45) NULL,
  `add_donor_info` JSON NULL DEFAULT NULL,
  `species_id` VARCHAR(20) NOT NULL, # TODO: Replace with CURIE-based reference, resolve FK in Library DB schema
  PRIMARY KEY (`donor_id`),
  CONSTRAINT `uq_entry` UNIQUE (`donor_identifier` ASC, `project` ASC),
  CONSTRAINT `chk_json_background_treatment` CHECK (json_valid(`background_treatment`)),
  CONSTRAINT `chk_json_add_donor_info` CHECK (json_valid(`add_donor_info`)))
COMMENT 'Metadata regarding the subject'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `sample`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sample` (
  `sample_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `donor_id` INT UNSIGNED NOT NULL,
  `tissue` VARCHAR(45) NOT NULL, # TODO: Replace with CURIE-based reference, resolve FK in Library DB schema
  `sampling_date` DATETIME NULL,
  `add_sample_info` JSON NULL DEFAULT NULL,
  PRIMARY KEY (`sample_id`),
  CONSTRAINT `uq_entry` UNIQUE (`donor_id` ASC, `sampling_date` ASC, `tissue` ASC, `add_sample_info`(250) ASC),
  CONSTRAINT `chk_json_add_sample_info` CHECK (json_valid(`add_sample_info`)),
  CONSTRAINT `fk_sample_donor_1` FOREIGN KEY (donor_id) REFERENCES donor (donor_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Metadata regarding the sample'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `sort`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sort` (
  `sort_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `antigen` VARCHAR(45) NULL COMMENT 'Use to annotate labeled baits used to isolate cells',
  `population` VARCHAR(45) NOT NULL,
  `sorting_date` DATETIME NULL,
  `add_sort_info` JSON NULL DEFAULT NULL,
  `sample_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`sort_id`),
  CONSTRAINT `uq_entry` UNIQUE (`sample_id` ASC, `sorting_date` ASC, `antigen` ASC, `population` ASC),
  CONSTRAINT `chk_json_add_sort_info` CHECK (json_valid(`add_sort_info`)),
  CONSTRAINT `fk_sort_sample_1` FOREIGN KEY (sample_id) REFERENCES sample (sample_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Metadata regarding the cell population and its isolation (assuming flow cytometric sorting)'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `event` (
  `event_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `well` INT NOT NULL,
  `plate` INT NOT NULL,
  `row` INT NOT NULL,
  `col` INT NOT NULL,
  `sequencing_run_sanger` VARCHAR(20) NULL COMMENT 'Optional for Sanger sequences',
  `add_event_info` JSON NULL DEFAULT NULL,
  `sort_id` INT UNSIGNED NOT NULL,
  `plate_layout_id` INT UNSIGNED NOT NULL, # TODO: Resolve FK in Library DB schema
  `plate_barcode` VARCHAR(45) NULL,
  PRIMARY KEY (`event_id`),
  CONSTRAINT `uq_entry` UNIQUE (`sort_id` ASC, `plate_barcode` ASC, `row` ASC, `col` ASC, `well` ASC, `plate` ASC),
  CONSTRAINT `chk_json_add_event_info` CHECK (json_valid(`add_event_info`)),
  CONSTRAINT `fk_event_sort_1` FOREIGN KEY (sort_id) REFERENCES sort (sort_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Main table for individual cells (assuming sorting into micro/nanotiter plates)'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `sequences`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sequences` (
  `seq_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(64) NULL,
  `consensus_rank` INT NULL,
  `locus` CHAR NULL,
  `length` INT UNSIGNED NOT NULL,
  `orient` CHAR NULL,
  `igblast_productive` TINYINT(1) NULL DEFAULT NULL,
  `seq` VARCHAR(1000) NOT NULL,
  `quality` VARCHAR(3000) NULL,
  `event_id` INT UNSIGNED NULL,
  PRIMARY KEY (`seq_id`),
  CONSTRAINT `uq_entry` UNIQUE (`event_id` ASC, `length` ASC, `locus` ASC, `name` ASC, `consensus_rank` ASC),
  CONSTRAINT `uq_name` UNIQUE (`name` ASC),
  CONSTRAINT `fk_sequence_event_1` FOREIGN KEY (event_id) REFERENCES event (event_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Main table for consensus sequences, referenced by all analysis results tables'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `constant_segments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `constant_segments` (
  `constant_segments_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `seq_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(20) NOT NULL,
  `percid` FLOAT NOT NULL,
  `length` INT UNSIGNED NOT NULL,
  `gapopens` INT UNSIGNED NOT NULL,
  `readstart` INT UNSIGNED NOT NULL,
  `readend` INT UNSIGNED NOT NULL,
  `eval` FLOAT NOT NULL,
  `score` FLOAT NOT NULL,
  `constant_id` INT UNSIGNED NOT NULL, # TODO: Resolve FK in Library DB schema
  PRIMARY KEY (`constant_segments_id`),
  CONSTRAINT `uq_seq_id` UNIQUE (`seq_id` ASC),
  CONSTRAINT `fk_constant_segments_sequences_1` FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Table for constant segment annotations of sequences'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `VDJ_segments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `VDJ_segments` (
  `VDJ_segments_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `seq_id` INT UNSIGNED NOT NULL,
  `type` CHAR(1) NOT NULL,
  `locus` CHAR NOT NULL,
  `igblast_rank` INT(1) UNSIGNED NOT NULL,
  `name` VARCHAR(20) NOT NULL,
  `eval` DOUBLE NOT NULL,
  `score` FLOAT NOT NULL,
  `VDJ_id` INT UNSIGNED NOT NULL, # TODO: Resolve FK in Library DB schema
  PRIMARY KEY (`VDJ_segments_id`),
  CONSTRAINT `uq_entry` UNIQUE (`seq_id` ASC, `type` ASC, `locus` ASC, `igblast_rank` ASC),
  CONSTRAINT `fk_VDJ_segments_sequences_1` FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Table for VDJ segment annotations of sequences'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `CDR_FWR`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `CDR_FWR` (
  `CDR_FWR_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `seq_id` INT UNSIGNED NOT NULL,
  `region` VARCHAR(20) NOT NULL,
  `start` INT UNSIGNED NOT NULL,
  `end` INT UNSIGNED NOT NULL,
  `dna_seq` VARCHAR(300) NOT NULL,
  `prot_seq` VARCHAR(100) NOT NULL,
  `prot_length` INT UNSIGNED NOT NULL,
  `stop_codon` TINYINT NOT NULL,
  PRIMARY KEY (`CDR_FWR_id`),
  CONSTRAINT `uq_entry` UNIQUE (`seq_id` ASC, `region` ASC, `start` ASC),
  CONSTRAINT `fk_CDR_FWR_sequences_1` FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Table for CDR/FWR delineation of sequences'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `warnings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `warnings` (
  `warnings_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `seq_id` INT UNSIGNED NOT NULL,
  `quality_score` INT NULL,
  `FWR3_igblast_output` TINYINT(1) NOT NULL,
  `CDR3_start_C` TINYINT(1) NOT NULL,
  `CDR3_end` TINYINT(1) NOT NULL,
  `alt_CDR3_end` TINYINT(1) NOT NULL,
  `J_end` TINYINT(1) NOT NULL,
  PRIMARY KEY (`warnings_id`),
  CONSTRAINT `fk_warnings_sequences_1` FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Table for warnings of the built-in VDJ joint analysis script'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `sequencing_run`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sequencing_run` (
  `sequencing_run_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` DATETIME NOT NULL,
  `name` VARCHAR(45) NULL,
  `processed_by` VARCHAR(45) NOT NULL,
  `experiment_id` VARCHAR(10) NULL,
  `add_sequencing_info` JSON NULL DEFAULT NULL,
  `add_pcr_info` JSON NULL DEFAULT NULL,
  `plate_layout_id` INT UNSIGNED NOT NULL, # TODO: Resolve FK in Library DB schema
  PRIMARY KEY (`sequencing_run_id`),
  CONSTRAINT `uq_entry` UNIQUE (`date` ASC, `name` ASC, `add_sequencing_info`(250) ASC, `processed_by` ASC),
  CONSTRAINT `chk_json_add_sequencing_info` CHECK (json_valid(`add_sequencing_info`)),
  CONSTRAINT `chk_json_add_pcr_info` CHECK (json_valid(`add_pcr_info`)))
COMMENT 'Metadata regarding the sequencing run (actually a single lane or multiplex component). For Matrix scPCR this is typically identical to a single pool (i.e. single locus), therefore the same experiment_id may be present in multiple runs.'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `consensus_stats`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `consensus_stats` (
  `consensus_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `sequences_seq_id` INT UNSIGNED NULL,
  `locus` CHAR NOT NULL,
  `n_seq` INT NULL DEFAULT 0,
  `best_V` INT NULL,
  `best_J` INT NULL,
  `col_tag` VARCHAR(45) NOT NULL,
  `row_tag` VARCHAR(45) NOT NULL,
  `experiment_id` VARCHAR(10) NOT NULL COMMENT 'Required to distinguish reads that map to the same well but come from different sequencing runs. Reads with the same experiment_id will be assembled into one consensus, irrespective of sequencing_run_id.',
  PRIMARY KEY (`consensus_id`),
  CONSTRAINT `uq_entry` UNIQUE (`col_tag` ASC, `row_tag` ASC, `locus` ASC, `experiment_id` ASC, `best_J` ASC, `best_V` ASC),
  CONSTRAINT `fk_consensus_stats_sequences_1` FOREIGN KEY (sequences_seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Table aggregating information on consensus builds. Note that the records in this table hold all consensus builds that were performed, will the sequences table only holds the top 2, ranked by their n_seq.'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `reads`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `reads` (
  `seq_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(64) NOT NULL,
  `locus` CHAR NULL,
  `length` INT UNSIGNED NOT NULL,
  `orient` CHAR NULL,
  `igblast_productive` TINYINT(1) NULL DEFAULT NULL,
  `seq` VARCHAR(1000) NOT NULL,
  `quality` VARCHAR(3000) NOT NULL,
  `sequencing_run_id` INT UNSIGNED NOT NULL,
  `well_id` INT(7) ZEROFILL NULL,
  `consensus_id` INT UNSIGNED NULL,
  PRIMARY KEY (`seq_id`),
  CONSTRAINT `uq_name` UNIQUE (`name` ASC),
  CONSTRAINT `fk_reads_sequencing_run_1` FOREIGN KEY (sequencing_run_id) REFERENCES sequencing_run (sequencing_run_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_reads_consensus_stats_1` FOREIGN KEY (consensus_id) REFERENCES consensus_stats (consensus_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Main table for raw sequencing reads, referenced by all intermeditate analysis tables prefix by "reads_"'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `reads_constant_segments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `reads_constant_segments` (
  `reads_constant_segments_id` INT(11) NOT NULL AUTO_INCREMENT,
  `seq_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(20) NOT NULL,
  `percid` FLOAT NOT NULL,
  `length` INT NOT NULL,
  `gapopens` INT NOT NULL,
  `readstart` INT NOT NULL,
  `readend` INT NOT NULL,
  `eval` FLOAT NOT NULL,
  `score` FLOAT NOT NULL,
  `constant_id` INT UNSIGNED NOT NULL, # TODO: Resolve FK in Library DB schema
  PRIMARY KEY (`reads_constant_segments_id`),
  CONSTRAINT `uq_seq_id` UNIQUE (`seq_id` ASC),
  CONSTRAINT `fk_reads_constant_segments_reads_1` FOREIGN KEY (seq_id) REFERENCES `reads` (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Table for constant segment annotations of reads'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `reads_VDJ_segments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `reads_VDJ_segments` (
  `reads_VDJ_segments_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `seq_id` INT UNSIGNED NOT NULL,
  `type` CHAR(1) NOT NULL,
  `locus` CHAR NOT NULL,
  `igblast_rank` INT UNSIGNED NOT NULL,
  `name` VARCHAR(20) NOT NULL,
  `eval` DOUBLE NOT NULL,
  `score` FLOAT NOT NULL,
  `VDJ_id` INT UNSIGNED NOT NULL, # TODO: Resolve FK in Library DB schema
  PRIMARY KEY (`reads_VDJ_segments_id`),
  CONSTRAINT `uq_entry` UNIQUE (`seq_id` ASC, `type` ASC, `locus` ASC, `igblast_rank` ASC),
  CONSTRAINT `fk_reads_VDJ_segments_reads_1` FOREIGN KEY (seq_id) REFERENCES `reads` (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Table for VDJ segment annotations of reads'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `reads_tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `reads_tags` (
  `reads_tags_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `seq_id` INT UNSIGNED NOT NULL,
  `percid` FLOAT NOT NULL,
  `direction` CHAR NOT NULL,
  `insertion` INT UNSIGNED NULL,
  `deletion` INT UNSIGNED NULL,
  `replacement` INT UNSIGNED NULL,
  `start` INT UNSIGNED NOT NULL,
  `end` INT UNSIGNED NOT NULL,
  `tag_id` INT UNSIGNED NOT NULL, # TODO: Resolve FK in Library DB schema
  PRIMARY KEY (`reads_tags_id`),
  CONSTRAINT `uq_entry` UNIQUE (`seq_id` ASC, `tag_id` ASC, `start` ASC),
  CONSTRAINT `fk_reads_tags_reads_1` FOREIGN KEY (seq_id) REFERENCES `reads` (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Table for tags identified in reads. For Matrix scPCR these are 16 bp barcodes on both ends of the amplicon'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `log_table`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `log_table` (
  `log_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` DATETIME NOT NULL,
  `pipeline_version` VARCHAR(45) NOT NULL,
  `user` VARCHAR(45) NOT NULL,
  `dbuser` VARCHAR(45) NOT NULL,
  `command` VARCHAR(100) NOT NULL,
  `output` BLOB NULL,
  PRIMARY KEY (`log_id`))
COMMENT 'General log table. DEPRECATED'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `mutations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mutations` (
  `mutations_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `seq_id` INT UNSIGNED NOT NULL,
  `position_codonstart_on_seq` INT UNSIGNED NOT NULL,
  `replacement` INT NOT NULL DEFAULT 0,
  `silent` INT NOT NULL DEFAULT 0,
  `insertion` INT NOT NULL DEFAULT 0,
  `deletion` INT NOT NULL DEFAULT 0,
  `undef_add_mutation` INT NOT NULL DEFAULT 0,
  `stop_codon_germline` INT NOT NULL DEFAULT 0,
  `stop_codon_sequence` INT NOT NULL DEFAULT 0,
  `in_status` INT NOT NULL DEFAULT 0,
  `del_status` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`mutations_id`),
  CONSTRAINT `uq_entry` UNIQUE (`seq_id` ASC, `position_codonstart_on_seq` ASC),
  CONSTRAINT `fk_mutations_sequences_1` FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Annotations of mismatches of a sequence versus the best reference sequence. For Ig sequences these are typically assumed to be somatic hypermutations (SHM)'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `igblast_alignment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `igblast_alignment` (
  `igblast_alignment_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `seq_id` INT UNSIGNED NOT NULL,
  `query_start` INT UNSIGNED NOT NULL,
  `germline_start` INT UNSIGNED NOT NULL,
  `query_seq` VARCHAR(500) NOT NULL,
  `germline_seq` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`igblast_alignment_id`),
  CONSTRAINT `uq_entry` UNIQUE (`seq_id` ASC),
  CONSTRAINT `fk_igblast_alignment_sequences_1` FOREIGN KEY (seq_id) REFERENCES sequences (seq_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'IgBLAST alignment output of a sequence versus the best reference sequence.'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `flow_meta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flow_meta` (
  `channel_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `detector_name` VARCHAR(45) NULL,
  `detector_scale` VARCHAR(45) NULL,
  `detector_spec` VARCHAR(45) NULL,
  `detector_voltage` INT NULL,
  `marker_name` VARCHAR(45) NULL,
  `marker_fluorochrome` VARCHAR(45) NULL,
  `sort_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`channel_id`),
  CONSTRAINT `fk_flow_meta_sort_1` FOREIGN KEY (sort_id) REFERENCES sort (sort_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Table to describe individual channels of a flow cytometric measurement'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `flow`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `flow` (
  `flow_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_id` INT UNSIGNED NOT NULL,
  `value` FLOAT NULL,
  `channel_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`flow_id`),
  CONSTRAINT `uq_entry` UNIQUE (`event_id`,`channel_id`),
  CONSTRAINT `fk_flow_event_1` FOREIGN KEY (event_id) REFERENCES event (event_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_flow_flow_meta_1` FOREIGN KEY (channel_id) REFERENCES flow_meta (channel_id) ON DELETE RESTRICT ON UPDATE CASCADE)
COMMENT 'Table for individual flow cytometric measurements pertaining to a sorted cell. Note that data is expected to be compensated before storing it in the DB.'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;
