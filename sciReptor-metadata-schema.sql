-- -----------------------------------------------------
-- Table `studies`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `studies` (
  `study_id` CHAR(8) NOT NULL COMMENT 'Calculated as the first 8 chars (i.e., 40 bits) of base32(sha3-256(database_name))',
  `database_name` VARCHAR(64) NOT NULL,
  `study_title` VARCHAR(1000) DEFAULT NULL,
  `study_type` VARCHAR(250) DEFAULT NULL,
  `inclusion_exclusion_criteria` VARCHAR(1000) DEFAULT NULL,
  `grants` VARCHAR(1000) DEFAULT NULL,
  `study_contact` VARCHAR(1000) DEFAULT NULL,
  `collected_by` VARCHAR(1000) NOT NULL,
  `lab_name` VARCHAR(1000) DEFAULT NULL,
  `lab_address` VARCHAR(1000) DEFAULT NULL,
  `submitted_by` VARCHAR(1000) NOT NULL,
  `pub_ids` VARCHAR(1000) DEFAULT NULL,
  `accession_ids` VARCHAR(1000) DEFAULT NULL,
  `keywords_study` VARCHAR(1000) NOT NULL,
  `single_cell` tinyint(1) NOT NULL,
  `cell_isolation` VARCHAR(100) DEFAULT NULL,
  `matrix_type` VARCHAR(12) DEFAULT NULL,
  PRIMARY KEY (`study_id`),
  CONSTRAINT `uq_database_name` UNIQUE (`database_name` ASC))
COMMENT 'Table for study metadata'
CHARACTER SET utf8
COLLATE utf8_general_ci
ENGINE InnoDB;

SHOW WARNINGS;
